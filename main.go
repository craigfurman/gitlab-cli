package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/xanzy/go-gitlab"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app        = kingpin.New("lab", "GitLab CLI.")
	apiBaseURL = app.Flag("api-base-url", "GitLab API base URL").Envar("GITLAB_API_BASE_URL").
			Default("https://gitlab.com/api/v4").String()
	token = app.Flag("api-token", "GitLab API token.").Envar("GITLAB_API_TOKEN").
		Default("").String()

	projects              = app.Command("projects", "Work with projects.")
	listProjects          = projects.Command("list", "List projects.").Alias("ls")
	listProjectsNamespace = listProjects.Arg("namespace", "User or group for which to list projects.").Required().String()
	listProjectsRecursive = listProjects.Flag("recursive", "Also include projects from subgroups, recursively.").
				Short('r').Default("false").Bool()
	listProjectsFilter = listProjects.Flag("filter", "Fields to output.").Short('f').
				Default("path,visibility,name,pathWithNamespace").String()

	defaultListOpts = gitlab.ListOptions{PerPage: apiPageMaximum}
)

const (
	apiPageMaximum = 100
)

type cli struct {
	client    *gitlab.Client
	outputter *Outputter
}

func main() {
	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	client := gitlab.NewClient(nil, *token)
	must(client.SetBaseURL(*apiBaseURL))
	c := &cli{
		client:    client,
		outputter: &Outputter{output: os.Stdout},
	}

	switch cmd {
	case listProjects.FullCommand():
		must(c.projectsList(*listProjectsNamespace, strings.Split(*listProjectsFilter, ",")))
	default:
		panic(fmt.Errorf("cmd %s unrecognised", cmd))
	}
}

func (c *cli) projectsList(namespace string, filter []string) error {
	user, err := c.findUser(namespace)
	if err != nil {
		return err
	}
	if user != nil {
		return c.getUserProjects(user.ID, filter)
	}

	group, err := c.findGroup(namespace)
	if err != nil {
		return err
	}
	if group == nil {
		return fmt.Errorf("no user or group found with full path: %s", namespace)
	}
	return c.getGroupProjects(group.ID, filter)
}

func (c *cli) findUser(name string) (*gitlab.User, error) {
	opts := &gitlab.ListUsersOptions{Search: &name, ListOptions: defaultListOpts}
	for {
		users, resp, err := c.client.Users.ListUsers(opts)
		if err != nil {
			return nil, err
		}
		for _, user := range users {
			if user.Username == name {
				return user, nil
			}
		}
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return nil, nil
}

func (c *cli) findGroup(name string) (*gitlab.Group, error) {
	hierarchyParts := strings.Split(name, "/")
	subgroupName := hierarchyParts[len(hierarchyParts)-1]

	opts := &gitlab.ListGroupsOptions{Search: &subgroupName, ListOptions: defaultListOpts}
	for {
		groups, resp, err := c.client.Groups.ListGroups(opts)
		if err != nil {
			return nil, err
		}
		for _, group := range groups {
			if group.FullPath == name {
				return group, nil
			}
		}
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return nil, nil
}

func (c *cli) getUserProjects(uid int, filter []string) error {
	var projects []*gitlab.Project
	opts := &gitlab.ListProjectsOptions{ListOptions: defaultListOpts}
	for {
		projectsPage, resp, err := c.client.Projects.ListUserProjects(uid, opts)
		if err != nil {
			return err
		}
		projects = append(projects, projectsPage...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return c.outputter.printList(projects, filter...)
}

func (c *cli) getGroupProjects(gid int, filter []string) error {
	var projects []*gitlab.Project
	opts := &gitlab.ListGroupProjectsOptions{IncludeSubgroups: listProjectsRecursive, ListOptions: defaultListOpts}
	for {
		projectsPage, resp, err := c.client.Groups.ListGroupProjects(gid, opts)
		if err != nil {
			return err
		}
		projects = append(projects, projectsPage...)
		if resp.CurrentPage >= resp.TotalPages {
			break
		}
		opts.Page = resp.NextPage
	}
	return c.outputter.printList(projects, filter...)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}

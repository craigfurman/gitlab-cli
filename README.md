# gitlab-cli

Very early WIP.

A thin CLI over https://github.com/xanzy/go-gitlab.

This doesn't do much yet. I'm only building things I need in shell functions,
such as listing all projects in a namespace so that I can synchronize a
directory.

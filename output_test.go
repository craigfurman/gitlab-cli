package main

import (
	"bytes"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xanzy/go-gitlab"
)

func TestPrintList(t *testing.T) {
	for _, tc := range []struct {
		name           string
		input          interface{}
		fields         []string
		expectedOutput string
		expectedErr    string
	}{
		{
			name:           "prints formatted table from list of structs",
			input:          []gitlab.User{user1, user2},
			fields:         []string{"id", "username", "isadmin", "createdAt"},
			expectedOutput: expectedOutputUsers,
		},
		{
			name:           "prints formatted table from list of struct pointers",
			input:          []*gitlab.User{&user1, &user2},
			fields:         []string{"id", "username", "isadmin", "createdAt"},
			expectedOutput: expectedOutputUsers,
		},
		{
			name:        "returns error when input is not a slice",
			input:       "wrong",
			expectedErr: "expected slice, got string",
		},
		{
			name:        "returns error when nonexistent field requested",
			input:       []*gitlab.User{&user1, &user2},
			fields:      []string{"id", "username", "idontexist"},
			expectedErr: "field idontexist not found",
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			var sink bytes.Buffer
			o := &Outputter{output: &sink}
			err := o.printList(tc.input, tc.fields...)
			if tc.expectedErr == "" {
				require.Nil(t, err)
			} else {
				require.EqualError(t, err, tc.expectedErr)
			}
			assert.Equal(t, strings.TrimSpace(tc.expectedOutput), strings.TrimSpace(sink.String()))
		})
	}
}

var (
	date  = time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
	user1 = gitlab.User{
		ID: 42, Username: "alice", IsAdmin: true,
		CreatedAt: &date,
	}
	user2 = gitlab.User{
		ID: 43, Username: "bob", IsAdmin: true,
		CreatedAt: &date,
	}

	expectedOutputUsers = `
ID  USERNAME  ISADMIN  CREATEDAT
42  alice     true     2009-11-10 23:00:00 +0000 UTC
43  bob       true     2009-11-10 23:00:00 +0000 UTC
`
)

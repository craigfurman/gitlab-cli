package main

import (
	"fmt"
	"io"
	"reflect"
	"strings"
	"text/tabwriter"
)

type Outputter struct {
	output io.Writer
}

func (o *Outputter) printList(list interface{}, fields ...string) error {
	listVal := reflect.ValueOf(list)
	if listVal.Kind() != reflect.Slice {
		return fmt.Errorf("expected slice, got %s", listVal.Kind().String())
	}

	tableWriter := tabwriter.NewWriter(o.output, 0, 0, 2, ' ', 0)

	for i, field := range fields {
		fmt.Fprint(tableWriter, strings.ToUpper(field))
		if i < len(fields)-1 {
			fmt.Fprint(tableWriter, "\t")
		}
	}
	fmt.Fprintln(tableWriter)

	for i := 0; i < listVal.Len(); i++ {
		element := listVal.Index(i)
		if element.Kind() == reflect.Ptr {
			element = reflect.Indirect(element)
		}
		elementType := element.Type()
		for fieldIdx, field := range fields {
			found := false
			for i := 0; i < element.NumField(); i++ {
				if strings.EqualFold(elementType.Field(i).Name, field) {
					val := element.Field(i).Interface()
					fmt.Fprintf(tableWriter, "%v", val)
					if fieldIdx < len(fields)-1 {
						fmt.Fprint(tableWriter, "\t")
					}
					found = true
				}
			}
			if !found {
				return fmt.Errorf("field %s not found", field)
			}
		}
		fmt.Fprintln(tableWriter)
	}

	return tableWriter.Flush()
}
